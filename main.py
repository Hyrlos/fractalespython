import library
import sys
from turtle import *


def koch_n(n, length):

    if n == 0 :
        forward(length)
    else:
        koch_n(n-1, length/3)
        left(60)
        koch_n(n-1, length/3)
        right(120)
        koch_n(n-1, length/3)
        left(60)
        koch_n(n-1, length/3)

def kochFlake(n, length):
    for i in range(6):

        koch_n(n, length)
        right(60)

def sierpinski(n, length):
    if n==0:
        for i in range(3):
            forward(length)
            left(120)
    if n > 0:
        sierpinski(n-1, length/2)
        forward(length/2)
        sierpinski(n-1, length/2)
        backward(length/2)
        left(60)
        forward(length/2)
        right(60)
        sierpinski(n-1, length/2)
        left(60)
        backward(length/2)
        right(60)

def main():
    if len(sys.argv) > 1:
        if sys.argv[1] == "sier":
            library.setupWindow()
            library.moveLeft(300)
            library.moveUp(-200)
            library.gotoTurtle(-400, -250)
            sierpinski(int(sys.argv[2]), int(sys.argv[3]))

        if sys.argv[1] == "koch":
            library.setupWindow()
            library.moveLeft(300)
            library.moveUp(-200)
            kochFlake(int(sys.argv[2]), int(sys.argv[3]))
    else:
        library.setupWindow()
        print("Default koch")
        print("python main.py sier n length")

        kochFlake(4, 300)
        library.gotoTurtle(-150, 190)

        kochFlake(5, 200)
        library.gotoTurtle(-100, 150)

        kochFlake(6, 100)
    input()

if __name__ == "__main__":
    # execute only if run as a script
    main()
