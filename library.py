from turtle import *

def moveLeft(width):
    left(180)
    penup()
    forward(width)
    pendown()
    left(180)

def moveUp(width):
    right(180)
    penup()
    forward(width)
    pendown()
    right(180)

def setupWindow():
    # Window : (length, width, positionOfTheWindowX, positionOfTheWindowX)
    setup(1000, 750, 10, 10)

    # turtle settings
    speed(0)
    color('brown', 'grey')
    hideturtle()

    # turtle position
    gotoTurtle(-200, 250)

def gotoTurtle(posX, posY):
    penup()
    goto(posX, posY)
    pendown()
