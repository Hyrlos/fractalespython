# Testing of the turtle API.

### Commands:

`python main.py`  
`python main.py sier n length`
`python main.py koch n length`  
`python drawTest.py`  
- n, the recursion's number
- length, the figure's length

example:
`python main.py sier 5 700`
`python main.py koch 4 300`

Per default (without arguments), the koch flake's fratale is start
