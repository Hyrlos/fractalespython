from turtle import *
color('red', 'brown')
begin_fill()
speed(0)
forward(200)
left(144)
while not abs(pos()) < 1:
    forward(200)
    left(144)
end_fill()
speed(3)
left(72)
for i in range(5):
    forward(122)
    right(72)
done()
